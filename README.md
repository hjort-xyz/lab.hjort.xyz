# lab.hjort.xyz

Fun PWA stuff

[Try the live demo](https://lab.hjort.xyz)
## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm start

# build for production with minification
npm run build
```
