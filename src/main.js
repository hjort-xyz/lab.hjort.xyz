// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.css';

import App from './App';
import Index from './views/Index';
import Notifications from './views/Notifications';
import Vibration from './views/Vibration';
import BrowserInfo from './views/BrowserInfo';
import DeviceInfo from './views/DeviceInfo';

import runtime from 'serviceworker-webpack-plugin/lib/runtime';
require('./manifest.json');

if ('serviceWorker' in navigator) {
	runtime.register();
}

Vue.use(VueRouter);
Vue.use(VueMaterial);

Vue.material.registerTheme('default', {
	primary: {
		color: 'green',
		hue: 700
	}
});

const routes = [
	{ path: '/', component: Index },
	{ path: '/notifications', component: Notifications },
	{ path: '/vibration', component: Vibration },
	{ path: '/browser-info', component: BrowserInfo },
	{ path: '/device-info', component: DeviceInfo }
];

const router = new VueRouter({
	mode: 'history',
	routes // short for routes: routes
});

/* eslint-disable no-new */
new Vue({
	el: '#app',
	template: '<App/>',
	router,
	components: { App }
});
